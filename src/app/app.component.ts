import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  links: Array<{ text: string, path: string }> = [];

  constructor(private router: Router) {

    this.router.config.unshift(
      { path: 'page1', component: Page1Component },
      { path: 'page2', component: Page2Component }
    );

    this.links.push(
      { text: 'page1', path: 'page1' },
      { text: 'page2', path: 'page2' },
    );
  }

}
